<?php

namespace Routes;

use Slim\App;
use Controller\AuthenticationController;

class AuthenticationRoute {

    public function __construct(App $app) {
        $this->createRoutes($app);
    }

    private function createRoutes(App $app) {
        $app->group("/auth", function() {
            $this->post('/login/', AuthenticationController::class . ':login');
            $this->post('/refresh_token/', AuthenticationController::class . ':refreshToken');
            $this->post('/logout/', AuthenticationController::class . ':logout');
        });
    }

}
