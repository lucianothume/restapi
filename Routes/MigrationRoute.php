<?php

namespace Routes;

use Slim\App;
use Controller\MigrationController;

class MigrationRoute {

    public function __construct(App $app) {
        $this->createRoutes($app);
    }

    private function createRoutes(App $app) {
        $self = $this;
        $app->group("/migration", function() use($self) {
            $this->get('/list/', MigrationController::class . ':getList');
            $this->get('/runAll/', MigrationController::class . ':runAll');
            $this->get('/pretend/', MigrationController::class . ':pretend');
            $this->get('/rollback/', MigrationController::class . ':rollback');
        });
    }

}
