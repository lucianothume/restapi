<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateAuthSessionDatabase extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('accesstokens', function (Blueprint $table) {
            $table->string('identifier')->primary();
            $table->timestamp('expiryDateTime')->useCurrent();
            $table->integer("userIdentifier");
            $table->string('clientId');
        });
        Schema::create('refreshtokens', function (Blueprint $table) {
            $table->string('identifier')->primary();
            $table->timestamp('expiryDateTime')->useCurrent();
            $table->string('accessTokenId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('refreshtokens');
        Schema::dropIfExists('accesstokens');
    }

}
