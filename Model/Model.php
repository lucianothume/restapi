<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Support\Carbon;

class Model extends BaseModel {

    public static function getRelationships() {
        return [];
    }

    /**
     * Formata a data que sera retornado para a aplicacao
     * @param \DateTimeInterface $date
     * @return string
     */
    protected function serializeDate(\DateTimeInterface $date) {
        return $date->format('d/m/Y H:i:s');
    }

    /**
     * Transforma em objeto a data que vem da aplicacao
     * @param string $value
     * @return Carbon
     */
    protected function asDateTime($value) {
        if (is_string($value)) {
            if (preg_match('/^(\d{4})-(\d{1,2})-(\d{1,2}) (\d{2}):(\d{2}):(\d{2})$/', $value)) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $value);
            }
            if (preg_match('/^(\d{1,2})\/(\d{1,2})\/(\d{4}) (\d{2}):(\d{2}):(\d{2})$/', $value)) {
                return Carbon::createFromFormat('d/m/Y H:i:s', $value);
            }
        }
        return parent::asDateTime($value);
    }

}
