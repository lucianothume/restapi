<?php

namespace Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use Model\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\Paginator;
use Core\Logger;

/**
 * Classe abstrata Controller com tratamento genérico para as requisições HTTP REST
 */
abstract class AbstractController implements ControllerInterface {

    /** @var string classe Model */
    protected $model;

    /** @var integer numero de registros por pagina */
    protected $per_page;

    /**
     * Construtor da classe abstrata Controller
     * @param string $model Nome da classe Model
     */
    protected function __construct($model) {
        $this->model = $model;
        $this->per_page = 25;
    }

    /**
     * Trata a requisição HTTP DELETE
     * @param Request $request
     * @param Response $response
     */
    public function delete(Request $request, Response $response, array $args) {
        try {
            $this->deleteById($args['id']);
            return $response->withStatus(204);
        } catch (\Exception $e) {
            $response->json(['error' => $e->getMessage()]);
            Logger::error($this->model . " delete", $e);
        }
    }

    /**
     * Realiza a exclusão da entidade
     * @param type $id
     */
    protected function deleteById($id) {
        $this->model::destroy($id);
    }

    /**
     * Trata a requisição HTTP GET com Id da entidade
     * @param Request $request
     * @param Response $response
     */
    public function getById(Request $request, Response $response, array $args) {
        $fields = $this->handleFields($request);
        /* @var $builder Builder */
        $builder = $this->model::query();
        $this->withRelationships($builder, $fields);
        $item = $builder->find($args['id'], $fields);
        return $response->withJson($item);
    }

    /**
     * Trata a requisição HTTP GET para listar as entidades
     * @param Request $request
     * @param Response $response
     */
    public function getList(Request $request, Response $response, array $args) {
        $fields = $this->handleFields($request);
        /* @var $query Builder */
        $query = $this->model::query();
        $this->withRelationships($query, $fields);
        $this->handleFilters($query, $request->getParam('filters'));
        $this->handleWhere($query, $request->getParam('where'));
        $this->handleSearch($query, $request->getParam('search'));
        $this->handleSort($query, $request->getParam('sort'));
        $this->handlePagination($request, $query);
        $result = $query->get($fields);
        return $response->withJson($result);
    }

    /**
     * Define as relações especificas para entidade
     */
    protected function withRelationships(Builder $query, &$fields) {
        $relationships = $this->model::getRelationships();
        if (count($fields) === 1 && $fields[0] === '*') {
            $query->with($relationships);
            return;
        }
        $withRelations = [];
        foreach ($fields as $field) {
            if (in_array($field, $relationships)) {
                array_push($withRelations, $field);
            }
        }
        foreach ($withRelations as $relation) {
            if (($key = array_search($relation, $fields)) !== false) {
                unset($fields[$key]);
            }
        }
        $query->with($withRelations);
    }

    /**
     * 
     * @param Request $request
     * @return array
     */
    protected function handleFields(Request $request) {
        $fields = $request->getParam('fields', null);
        return $fields ? (is_array($fields) ? $fields : explode(',', $fields)) : ['*'];
    }

    /**
     * Trata a paginacao usando 'forPage'
     * @param Request $request
     * @param Builder $query
     * @return Builder
     */
    protected function handlePagination(Request $request, Builder $query) {
        $params = $request->getParams();
        $per_page = isset($params['per_page']) ? intval($params['per_page']) : $this->per_page;
        $pageNum = isset($params['page']) ? intval($params['page']) : 1;
        return $query->forPage($pageNum, $per_page);
    }

    /**
     * Trata a paginacao usando 'simplePaginate' retornando o Paginator
     * @param Request $request
     * @param Builder $query
     * @return Paginator
     */
    protected function handleSimplePagination(Request $request, Builder $query) {
        $params = $request->getParams();
        $per_page = isset($params['per_page']) ? intval($params['per_page']) : $this->per_page;
        $pageNum = isset($params['page']) ? intval($params['page']) : 1;
        return $query->simplePaginate($per_page, [], "page", $pageNum);
    }

    /**
     * Trata o 'search' da listagem
     * @param Builder $query
     * @param type $search
     * @return Builder
     */
    protected function handleSearch(Builder $query, $search) {
        if ($search === null || !is_array($search) || count($search) === 0) {
            return $query;
        }
        $where = [];
        foreach ($search as $key => $value) {
            $where[] = [$key, 'like', "%" . $value . "%"];
        }
        return $query->where($where);
    }

    /**
     * Trata o filtro da listagem
     * @param Builder $query
     * @param type $filters
     * @return Builder
     */
    protected function handleFilters(Builder $query, $filters) {
        if ($filters === null || !is_array($filters) || count($filters) === 0) {
            return $query;
        }
        $where = [];
        $whereIn = [];
        foreach ($filters as $key => $value) {
            $values = is_array($value) ? $value : explode(',', $value);
            if (count($values) > 1) {
                $whereIn[$key] = $values;
            } else {
                $where[] = [$key, '=', $value];
            }
        }
        $query->where($where);
        if (count($whereIn) > 0) {
            foreach ($whereIn as $key => $value) {
                $query->whereIn($key, $value);
            }
        }
        return $query;
    }

    /**
     * Trata o 'sort' da listagem
     * @param Builder $query
     * @param string $sort campos separados por virgula
     * @return Builder
     */
    protected function handleSort(Builder $query, $sort) {
        if ($sort === null) {
            return $query;
        }
        $sorts = is_array($sort) ? $sort : explode(',', $sort);
        foreach ($sorts as $value) {
            if (!$value) {
                continue;
            }
            $query = $value[0] === '-' ? $query->orderByDesc(substr($value, 1)) : $query->orderBy($value);
        }
        return $query;
    }

    /**
     * Trata condições personalizadas na clausula WHERE
     * @param Builder $query
     * @param type $where Ex: ?where[]=Id,>,5&where[]=Nome,=,Teste
     * @return Builder
     */
    protected function handleWhere(Builder $query, $where) {
        if ($where === null) {
            return $query;
        }
        $wheres = is_array($where) ? $where : [$where];
        foreach ($wheres as $whr) {
            if (!$whr) {
                continue;
            }
            list($column, $operator, $value) = explode(",", $whr);
            $query->where($column, $operator, $value);
        }
        return $query;
    }

    /**
     * Trata a requisição HTTP POST para inserir um novo registro
     * @param Request $request
     * @param Response $response
     */
    public function post(Request $request, Response $response, array $args) {
        $params = null;
        if ($this->isApplicationJson($request)) {
            $params = json_decode($request->getBody(), true);
        }
        if ($params !== null) {
            /* @var $item Model */
            $item = new $this->model($params);
            $item->save();
            return $response->withJson($item);
        } else {
            return $response->withStatus(400);
        }
        return $response;
    }

    /**
     * Trata a requisição HTTP PUT para atualizar um registro com id especificado
     * @param Request $request
     * @param Response $response
     */
    public function put(Request $request, Response $response, array $args) {
        /* @var $builder Builder */
        $builder = $this->model::query();
        /* @var $item Model */
        $item = $builder->find($args['id']);
        if ($item === null || !$this->isApplicationJson($request)) {
            return $response->withStatus(400);
        } else {
            $params = json_decode($request->getBody(), true);
            if ($item->update($params)) {
                return $response->withJson($item);
            } else {
                return $response->withStatus(400);
            }
        }
        return $response;
    }

    /**
     * Verifica se a requisição tem Content-Type = 'application/json'
     * @param Request $request
     * @return bool
     */
    private function isApplicationJson(Request $request) {
        $contentType = $request->getHeaderLine('content-type');
        return strpos($contentType, "application/json") !== false;
    }

}
