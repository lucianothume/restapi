<?php

namespace Core\OAuth2;

use Model\Model;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\Traits\ClientTrait;
use League\OAuth2\Server\Entities\Traits\EntityTrait;

class Client implements ClientEntityInterface {

    use ClientTrait;
    use EntityTrait;

}
