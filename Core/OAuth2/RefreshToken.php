<?php

namespace Core\OAuth2;

use Model\Model;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;

class RefreshToken extends Model implements RefreshTokenEntityInterface {

    protected $table = "refreshtokens";
    public $incrementing = false;
    protected $primaryKey = 'identifier';
    protected $keyType = 'string';
    protected $fillable = [
        "identifier",
        "expiryDateTime",
        "accessTokenId"
    ];
    public $timestamps = false;
    protected $dates = [
        "expiryDateTime"
    ];

    public function accessToken() {
        return $this->hasOne(AccessToken::class, 'identifier', 'accessTokenId');
    }

    public function getAccessToken() {
        return $this->accessToken;
    }

    public function setAccessToken(AccessTokenEntityInterface $accessToken) {
        $this->accessTokenId = $accessToken->getIdentifier();
    }

    /**
     * Get the token's expiry date time.
     *
     * @return \DateTime
     */
    public function getExpiryDateTime() {
        return $this->expiryDateTime;
    }

    /**
     * Set the date time when the token expires.
     *
     * @param \DateTime $dateTime
     */
    public function setExpiryDateTime(\DateTime $dateTime) {
        $this->expiryDateTime = $dateTime;
    }

    /**
     * @return mixed
     */
    public function getIdentifier() {
        return $this->identifier;
    }

    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

}
