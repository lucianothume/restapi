<?php

//spl_autoload_extensions(".php");

require './vendor/autoload.php';
require './config/database.php';

use Illuminate\Support\Facades\Schema;
use Core\RestAPIApp;

$api = new RestAPIApp();
Schema::setFacadeApplication($api);

$app = new \Slim\App();
$namespace = "";
$app->get('/', function (\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
    return $response->write("<h4>REST API</h4>");
});

$c = $app->getContainer();
$c['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $c['response']->withStatus(500)->withHeader('Content-Type', 'text/html')->write(var_dump($exception));
    };
};

$oAuthServer = new \Core\OAuth2\OAuth2Server();
$c['oAuthServer'] = $oAuthServer;
$c['oAuthResourceServer'] = (new \Core\OAuth2\AuthResourceServer())->getResourceServer();

$app->group($namespace, function() {
    new Routes\MigrationRoute($this);
    new Routes\AuthenticationRoute($this);
    new Routes\UserRoute($this);
    new Routes\CorsOptionsRoute($this);
});
$app->run();

