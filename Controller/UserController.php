<?php

namespace Controller;

use Model\User;

class UserController extends AbstractController {

    function __construct() {
        parent::__construct(User::class);
    }

}
