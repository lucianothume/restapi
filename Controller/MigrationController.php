<?php

namespace Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Database\Migrations\DatabaseMigrationRepository;
use Illuminate\Database\Migrations\Migrator;
use Model\Model;

class MigrationController {

    private $migrator;
    private $directory = __DIR__ . "/../Database/Migrations/";

    public function __construct() {
        $resolver = Model::getConnectionResolver();
        $repository = new DatabaseMigrationRepository($resolver, "migrations");
        $filesystem = new Filesystem();
        $this->migrator = new Migrator($repository, $resolver, $filesystem);
        $this->checkRepository();
    }

    private function checkRepository() {
        if (!$this->migrator->getRepository()->repositoryExists()) {
            $this->migrator->getRepository()->createRepository();
        }
    }

    public function getList(Request $request, Response $response) {
        return $response->withJson([
                    'all' => $this->migrator->getMigrationFiles($this->directory),
                    'ran' => $this->migrator->getRepository()->getRan()
        ]);
    }

    public function runAll(Request $request, Response $response) {
        $this->migrator->run([$this->directory]);
        return $this->respondMigrationNotes($response);
    }

    public function pretend(Request $request, Response $response) {
        $this->migrator->run([$this->directory], ['pretend' => true]);
        return $this->respondMigrationNotes($response);
    }

    public function rollback(Request $request, Response $response) {
        $this->migrator->rollback([$this->directory]);
        return $this->respondMigrationNotes($response);
    }

    private function respondMigrationNotes(Response $response) {
        $body = $response->getBody();
        foreach ($this->migrator->getNotes() as $note) {
            $body->write(str_replace('"', "'", $note));
            $body->write('<br><br>');
        }
        return $response->withBody($body);
    }

}
