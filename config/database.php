<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();

$manager = $capsule->getDatabaseManager();

$connDriver = 'mysql';
switch ($connDriver) {
    case 'sqlsrv':
        $capsule->addConnection([
            'driver' => 'sqlsrv',
            //'host' => env('DB_HOST', 'localhost'),
            'host' => env('DB_HOST', '127.0.0.1\SQLEXPRESS'),
            //'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'master'),
            'username' => env('DB_USERNAME', 'sa'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
        ]);
        break;    
    case 'mysql':
        $capsule->addConnection([
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'db',
            'username' => 'root',
            'password' => '',
            'prefix' => '',
            'port' => 3306
        ]);
        break;
}

$capsule->setAsGlobal();

$capsule->bootEloquent();
