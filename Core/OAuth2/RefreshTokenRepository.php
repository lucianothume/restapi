<?php

namespace Core\OAuth2;

use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use Core\OAuth2\RefreshToken;

class RefreshTokenRepository implements RefreshTokenRepositoryInterface {

    /**
     * Creates a new refresh token
     *
     * @return RefreshTokenEntityInterface
     */
    public function getNewRefreshToken() {
        $token = new RefreshToken();
        //$token->save();
        return $token;
    }

    /**
     * Check if the refresh token has been revoked.
     *
     * @param string $tokenId
     *
     * @return bool Return true if this token has been revoked
     */
    public function isRefreshTokenRevoked($tokenId) {
        /* @var $token RefreshToken */
        $token = RefreshToken::query()->find($tokenId);
        if ($token !== null) {
            $now = new \DateTime();
            return $now >= $token->getExpiryDateTime();
        }
        return true;
    }

    /**
     * Create a new refresh token_name.
     *
     * @param RefreshTokenEntityInterface $refreshTokenEntity
     *
     * @throws UniqueTokenIdentifierConstraintViolationException
     */
    public function persistNewRefreshToken(RefreshTokenEntityInterface $refreshTokenEntity) {
        $token = null;
        $id = $refreshTokenEntity->getIdentifier();
        if ($id) {
            $token = RefreshToken::query()->find($id);
        } else {
            $id = md5(uniqid(''));
        }
        if (!$token) {
            $token = new RefreshToken();
        }
        $token->setIdentifier($id);
        $token->setExpiryDateTime($refreshTokenEntity->getExpiryDateTime());
        $token->setAccessToken($refreshTokenEntity->getAccessToken());
        $token->save();
    }

    /**
     * Revoke the refresh token.
     *
     * @param string $tokenId
     */
    public function revokeRefreshToken($tokenId) {
        RefreshToken::destroy($tokenId);
    }

}
