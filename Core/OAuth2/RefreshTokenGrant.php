<?php

namespace Core\OAuth2;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class RefreshTokenGrant extends \League\OAuth2\Server\Grant\RefreshTokenGrant {

    public function __construct(\League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface $refreshTokenRepository) {
        parent::__construct($refreshTokenRepository);
    }

    public function revokeRefreshToken(ServerRequestInterface $request, ResponseInterface $response) {
        $client = $this->validateClient($request);
        $oldRefreshToken = $this->validateOldRefreshToken($request, $client->getIdentifier());
        // Expire old tokens
        $this->refreshTokenRepository->revokeRefreshToken($oldRefreshToken['refresh_token_id']);
        $this->accessTokenRepository->revokeAccessToken($oldRefreshToken['access_token_id']);        
        return $response->withStatus(204);
    }

}
