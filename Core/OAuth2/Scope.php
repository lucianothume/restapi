<?php

namespace Core\OAuth2;

use Model\Model;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Entities\Traits\EntityTrait;

class Scope implements ScopeEntityInterface {

    use EntityTrait;

    public function jsonSerialize() {
        return $this->getIdentifier();
    }

}
