<?php

namespace Core;

use Illuminate\Contracts\Foundation\Application;
use ArrayAccess;
use Closure;

class RestAPIApp implements Application, ArrayAccess {

    protected $db;

    public function __construct() {
        $this->db = \Model\Model::getConnectionResolver();
    }

    public function afterResolving($abstract, Closure $callback = null): void {
        
    }

    public function alias($abstract, $alias): void {
        
    }

    public function basePath(): string {
        
    }

    public function bind($abstract, $concrete = null, $shared = false): void {
        
    }

    public function bindIf($abstract, $concrete = null, $shared = false): void {
        
    }

    public function boot(): void {
        
    }

    public function booted($callback): void {
        
    }

    public function booting($callback): void {
        
    }

    public function bound($abstract): bool {
        
    }

    public function call($callback, array $parameters = array(), $defaultMethod = null) {
        
    }

    public function environment(): string {
        
    }

    public function extend($abstract, Closure $closure): void {
        
    }

    public function factory($abstract): Closure {
        
    }

    public function get($id) {
        
    }

    public function getCachedPackagesPath(): string {
        
    }

    public function getCachedServicesPath(): string {
        
    }

    public function has($id): bool {
        
    }

    public function instance($abstract, $instance) {
        
    }

    public function isDownForMaintenance(): bool {
        
    }

    public function make($abstract, array $parameters = array()) {
        
    }

    public function register($provider, $options = array(), $force = false): \Illuminate\Support\ServiceProvider {
        
    }

    public function registerConfiguredProviders(): void {
        
    }

    public function registerDeferredProvider($provider, $service = null): void {
        
    }

    public function resolved($abstract): bool {
        
    }

    public function resolving($abstract, Closure $callback = null): void {
        
    }

    public function runningInConsole(): bool {
        
    }

    public function singleton($abstract, $concrete = null): void {
        
    }

    public function tag($abstracts, $tags): void {
        
    }

    public function tagged($tag): array {
        
    }

    public function version(): string {
        
    }

    public function when($concrete): \Illuminate\Contracts\Container\ContextualBindingBuilder {
        
    }

    public function offsetExists($offset): bool {
        return isset($this->{$offset});
    }

    public function offsetGet($offset) {
        return ($this->{$offset});
    }

    public function offsetSet($offset, $value) {
        $this->{$offset} = $value;
    }

    public function offsetUnset($offset) {
        unset($this->{$offset});
    }

    public function runningUnitTests(): bool {
        
    }

}
