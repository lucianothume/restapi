<?php

namespace Routes;

use Slim\App;
use Controller\UserController;

class UserRoute extends BasicCrudRoute {

    function __construct(App $app) {
        parent::__construct($app, "/user", UserController::class);
    }
}