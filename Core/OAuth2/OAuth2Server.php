<?php

namespace Core\OAuth2;

use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\CryptKey;

class OAuth2Server {

    /** @var AuthorizationServer */
    private $server;

    /** @var RefreshTokenGrant */
    private $refreshTokenGrant;

    public function __construct() {

        $clientRepository = new ClientRepository();
        $scopeRepository = new ScopeRepository();
        $accessTokenRepository = new AccessTokenRepository();
        $userRepository = new UserRepository();
        $refreshTokenRepository = new RefreshTokenRepository();

        $accessTokenInterval = new \DateInterval('PT10M'); // access tokens will expire after 10 minutes
        $refreshTokenInterval = new \DateInterval('P1D'); // refresh tokens will expire after 1 Day
        // Path to public and private keys
        $privateKey = new CryptKey(__DIR__ . '/../../private.key', null, false); // if private key has a pass phrase
        $encryptionKey = 'svOZ/YfhoR283tGRCyh0DxYHPU0Ol749vU1UtMvffpk='; // generate using base64_encode(random_bytes(32))
        // Setup the authorization servers
        $this->server = new AuthorizationServer($clientRepository, $accessTokenRepository, $scopeRepository, $privateKey, $encryptionKey);
        $pswdGrant = new PasswordGrant($userRepository, $refreshTokenRepository);
        $pswdGrant->setRefreshTokenTTL($refreshTokenInterval);
        $this->server->enableGrantType($pswdGrant, $accessTokenInterval);

        $this->refreshTokenGrant = new RefreshTokenGrant($refreshTokenRepository);
        $this->refreshTokenGrant->setRefreshTokenTTL($refreshTokenInterval);
        $this->server->enableGrantType($this->refreshTokenGrant, $accessTokenInterval);
    }

    /**
     * 
     * @return AuthorizationServer
     */
    public function getAuthorizationServer() {
        return $this->server;
    }

    /**
     * 
     * @return RefreshTokenGrant
     */
    public function getRefreshTokenGrant() {
        return $this->refreshTokenGrant;
    }

}
