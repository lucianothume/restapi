<?php

namespace Controller;

use Slim\Http\Request;
use Slim\Http\Response;

interface ControllerInterface {

    public function delete(Request $request, Response $response, array $args);

    public function getById(Request $request, Response $response, array $args);

    public function getList(Request $request, Response $response, array $args);

    public function post(Request $request, Response $response, array $args);

    public function put(Request $request, Response $response, array $args);
}
