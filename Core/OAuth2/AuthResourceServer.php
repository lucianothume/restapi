<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\OAuth2;

use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\ResourceServer;
use Core\OAuth2\AccessTokenRepository;

class AuthResourceServer {

    /** @var ResourceServer */
    private $server;

    public function __construct() {
        $accessTokenRepository = new AccessTokenRepository();
        $publicKey = new CryptKey(__DIR__ . '/../../public.key', null, false);
        $this->server = new ResourceServer($accessTokenRepository, $publicKey);
    }

    public function getResourceServer() {
        return $this->server;
    }

}
