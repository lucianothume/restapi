<?php

namespace Routes;

use Slim\App;
use League\OAuth2\Server\Middleware\ResourceServerMiddleware;

class BasicCrudRoute {

    protected $path;

    /** @var string */
    private $controller;

    function __construct(App $app, $path, $controller) {
        $this->path = $path;
        $this->controller = $controller;
        $this->createRoutes($app);
    }

    /**
     * Retorna a classe do Controller
     * @return string
     */
    public function getController() {
        return $this->controller;
    }

    private function createRoutes(App $app) {
        $self = $this;
        $app->group($this->path, function() use ($self) {
            $this->get('/{id:[0-9]+}/', $self->getController() . ':getById');
            $this->get('/list/', $self->getController() . ':getList');
            $this->delete('/{id:[0-9]+}/', $self->getController() . ':delete');
            $this->post("/", $self->getController() . ':post');
            $this->put('/{id:[0-9]+}/', $self->getController() . ':put');
        })->add(new ResourceServerMiddleware($app->getContainer()->get('oAuthResourceServer')));
    }

}
