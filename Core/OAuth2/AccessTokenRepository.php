<?php

namespace Core\OAuth2;

use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use Core\OAuth2\AccessToken;

class AccessTokenRepository implements AccessTokenRepositoryInterface {

    /**
     * Create a new access token
     *
     * @param ClientEntityInterface  $clientEntity
     * @param ScopeEntityInterface[] $scopes
     * @param mixed                  $userIdentifier
     *
     * @return AccessTokenEntityInterface
     */
    public function getNewToken(ClientEntityInterface $clientEntity, array $scopes, $userIdentifier = null) {
        $token = new AccessToken();
        $token->setClient($clientEntity);
        $token->setUserIdentifier($userIdentifier);
        //$token->save();
        return $token;
    }

    /**
     * Check if the access token has been revoked.
     *
     * @param string $tokenId
     *
     * @return bool Return true if this token has been revoked
     */
    public function isAccessTokenRevoked($tokenId) {
        /* @var $token AccessToken */
        $token = AccessToken::query()->find($tokenId);
        if ($token !== null) {
            $now = new \DateTime();
            return $now >= $token->getExpiryDateTime();
        }
        return true;
    }

    /**
     * Persists a new access token to permanent storage.
     *
     * @param AccessTokenEntityInterface $accessTokenEntity
     *
     * @throws UniqueTokenIdentifierConstraintViolationException
     */
    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity) {
        $token = null;
        $id = $accessTokenEntity->getIdentifier();
        if ($id) {
            $token = AccessToken::query()->find($id);
        } else {
            $id = md5(uniqid(''));
        }
        if (!$token) {
            $token = new AccessToken();
        }
        $token->setIdentifier($id);
        $token->setClient($accessTokenEntity->getClient());
        $token->setUserIdentifier($accessTokenEntity->getUserIdentifier());
        $token->setExpiryDateTime($accessTokenEntity->getExpiryDateTime());
        $token->save();
    }

    /**
     * Revoke an access token.
     *
     * @param string $tokenId
     */
    public function revokeAccessToken($tokenId) {
        AccessToken::destroy($tokenId);
    }

}
