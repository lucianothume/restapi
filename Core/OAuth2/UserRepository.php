<?php

namespace Core\OAuth2;

use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\UserEntityInterface;
use Core\OAuth2\User as AuthUser;
use Model\User;
use Illuminate\Hashing\BcryptHasher;

class UserRepository implements UserRepositoryInterface {

    /**
     * Get a user entity.
     *
     * @param string                $username
     * @param string                $password
     * @param string                $grantType    The grant type used
     * @param ClientEntityInterface $clientEntity
     *
     * @return UserEntityInterface
     */
    public function getUserEntityByUserCredentials($username, $password, $grantType, ClientEntityInterface $clientEntity) {
        $builder = User::query();
        $builder->where("email", "=", $username);
        $users = $builder->get();
        if ($users !== null) {
            $hasher = new BcryptHasher();
            foreach ($users as $user) {
                if ($hasher->check($password, $user->password)) {
                    $authUser = new AuthUser();
                    $authUser->setIdentifier($user->id);
                    return $authUser;
                }
            }
        }
        return null;
    }

}
