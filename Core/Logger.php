<?php

namespace Core;

use Exception;

class Logger {

    protected static $path = __DIR__ . "/../logs/";

    public static function info($msg) {
        if (is_array($msg)) {
            foreach ($msg as $m) {
                self::info($m);
            }
            return;
        }
        $fileName = self::$path . "info.txt";
        $data = date("Y-m-d H:i:s") . ": " . $msg . PHP_EOL;
        file_put_contents($fileName, $data, file_exists($fileName) ? FILE_APPEND : 0);
    }

    public static function error($msg, Exception $e = null) {
        $fileName = self::$path . "error.txt";
        $data = date("Y-m-d H:i:s") . ": " . $msg . ($e ? " - " . $e->getMessage() : "") . PHP_EOL;
        if ($e) {
            foreach ($e->getTrace() as $trace) {
                $data .= json_encode($trace) . PHP_EOL;
            }
        }
        file_put_contents($fileName, $data, file_exists($fileName) ? FILE_APPEND : 0);
    }

}
