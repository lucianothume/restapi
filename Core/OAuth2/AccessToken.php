<?php

namespace Core\OAuth2;

use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use Model\Model;

class AccessToken extends Model implements AccessTokenEntityInterface {

    use \League\OAuth2\Server\Entities\Traits\AccessTokenTrait;

    protected $table = "accesstokens";
    public $incrementing = false;
    protected $primaryKey = 'identifier';
    protected $keyType = 'string';
    protected $fillable = [
        "identifier",
        "expiryDateTime",
        "userIdentifier",
        "clientId"
    ];
    public $timestamps = false;
    protected $dates = [
        "expiryDateTime"
    ];

    public function addScope(\League\OAuth2\Server\Entities\ScopeEntityInterface $scope) {
        
    }

    public function getClient() {
        $client = new Client();
        $client->setIdentifier($this->clientId);
        return $client;
    }

    public function getExpiryDateTime() {
        return $this->expiryDateTime;
    }

    public function getScopes() {
        $scope = new Scope();
        $scope->setIdentifier('RestApi');
        return [$scope];
    }

    public function getUserIdentifier() {
        return $this->userIdentifier;
    }

    public function setClient(\League\OAuth2\Server\Entities\ClientEntityInterface $client) {
        $this->clientId = $client->getIdentifier();
    }

    public function setExpiryDateTime(\DateTime $dateTime) {
        $this->expiryDateTime = $dateTime;
    }

    public function setUserIdentifier($identifier) {
        $this->userIdentifier = $identifier;
    }

    /**
     * @return mixed
     */
    public function getIdentifier() {
        return $this->identifier;
    }

    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

}
