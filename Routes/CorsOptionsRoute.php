<?php

namespace Routes;

use Slim\App;
use Tuupola\Middleware\CorsMiddleware;

class CorsOptionsRoute {

    function __construct(App $app) {
        /* $app->options('/[{routes:.*}]', function(Request $request, Response $response, array $args) {
          return $response;
          $response->withHeader('Access-Control-Allow-Origin', '*');
          $response->withHeader('Access-Control-Allow-Headers', 'Content-Type,Accept,Access-Control-Allow-Origin,Authorization');
          $response->withHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE,OPTIONS');
          return $response->withStatus(204);
          }); */
        $app->add(new CorsMiddleware([
            "origin" => ["*"],
            "methods" => ["GET", "POST", "PUT", "DELETE", "OPTIONS"],
            "headers.allow" => ['Content-Type', 'Accept', 'Access-Control-Allow-Origin', 'Authorization'],
            "headers.expose" => [],
            "credentials" => false,
            "cache" => 0,
        ]));
    }

}
