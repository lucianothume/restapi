<?php

namespace Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use Core\OAuth2\OAuth2Server;

class AuthenticationController {

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function login(Request $request, Response $response, array $args) {
        return $this->respondToAccessTokenRequest($request, $response, $args);
    }

    public function refreshToken(Request $request, Response $response, array $args) {
        return $this->respondToAccessTokenRequest($request, $response, $args);
    }

    public function logout(Request $request, Response $response, array $args) {
        try {
            /* @var $server OAuth2Server */
            $server = $this->container->get('oAuthServer');
            return $server->getRefreshTokenGrant()->revokeRefreshToken($request, $response);
        } catch (\League\OAuth2\Server\Exception\OAuthServerException $exception) {
            // All instances of OAuthServerException can be formatted into a HTTP response
            return $exception->generateHttpResponse($response);
        } catch (\Exception $exception) {
            return $response->withStatus(500)->withJson($exception);
        }
    }

    protected function respondToAccessTokenRequest(Request $request, Response $response, array $args) {
        try {
            /* @var $server OAuth2Server */
            $server = $this->container->get('oAuthServer');
            return $server->getAuthorizationServer()->respondToAccessTokenRequest($request, $response);
        } catch (\League\OAuth2\Server\Exception\OAuthServerException $exception) {
            // All instances of OAuthServerException can be formatted into a HTTP response
            return $exception->generateHttpResponse($response);
        } catch (\Exception $exception) {
            return $response->withStatus(500)->withJson($exception);
        }
    }

}
